# criando o projeto rails
rails new video_locadora

# acessando a pasta do projeto
cd video_locadora

Editem o arquivo Gemfile e adicionem:
	# gem install twitter-bootstrap-rails --no-rdoc --no-ri -u
	gem 'twitter-bootstrap-rails', '3.2.2'
	gem 'brazilian-rails', '3.3.0'

# instalando gems
bundle install

Como o Rails trabalha com plural, existem algumas palavras em português que podem ser problemas. Por exemplo: midia e locacao. Para corrigir isto vamos editar o arquivo config/initializers/inflections.rb
E incluir o seguinte trecho de código, aonde definiremos as palavras no singular e plural:
ActiveSupport::Inflector.inflections(:en) do |inflect| 
   inflect.irregular 'categoria', 'categorias' 
   inflect.irregular 'midia', 'midias' 
   inflect.irregular 'locacao', 'locacoes' 
end

# criando os scaffolds

rails g scaffold categorias descricao:string

rails g scaffold filmes categoria:references descricao:string ano:integer

rails g scaffold midias filme:references inutilizada:boolean

rails g scaffold clientes nome:string telefone:string celular:string email:string

rails g scaffold enderecos rua:string bairro:string numero:string  cidade:string estado:string cep:string complemento:string cliente:references

rails g scaffold locacoes cliente:references midia:references dia_emprestimo:date dia_devolucao:date observacao:string

# criando tabelas do banco de dados
rake db:migrate


# estilizando com bootstrap e recriando telas

rails generate bootstrap:install static --no-coffeescript

rails g bootstrap:layout --force

rails g bootstrap:themed categorias --force

rails g bootstrap:themed filmes --force

rails g bootstrap:themed midias --force

rails g bootstrap:themed clientes --force

rails g bootstrap:themed enderecos --force

rails g bootstrap:themed locacoes –force


# leiam a documentação
# https://github.com/seyhunak/twitter-bootstrap-rails
# https://github.com/tapajos/brazilian-rails

# Para montar os forms com mais facilidade entenda
# http://guides.rubyonrails.org/form_helpers.html



Para colocar um select para escolher a categoria do filme edite o arquivo app/views/filmes/_form.html.erb
e substitua:
<%= f.text_field :categoria_id, :class => 'form-control' %>
por:
<%= f.collection_select(:categoria_id, Categoria.all, :id, :descricao, {prompt: "Selecione"}, {class: 'form-control'}) %>


# Para usar autocomplete veja:
# https://github.com/peterwillcn/rails4-autocomplete
# Para usar o banco de dados Postgresql utilize a gem 'pg' no arquivo Gemfile e altere o arquivo config/database.yml
