json.extract! filme, :id, :categoria_id, :descricao, :ano, :created_at, :updated_at
json.url filme_url(filme, format: :json)