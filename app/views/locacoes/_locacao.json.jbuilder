json.extract! locacao, :id, :cliente_id, :midia_id, :dia_emprestimo, :dia_devolucao, :observacao, :created_at, :updated_at
json.url locacao_url(locacao, format: :json)