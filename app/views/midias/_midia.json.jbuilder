json.extract! midia, :id, :filme_id, :inutilizada, :created_at, :updated_at
json.url midia_url(midia, format: :json)