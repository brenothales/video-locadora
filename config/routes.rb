Rails.application.routes.draw do
  resources :locacoes
  resources :enderecos
  resources :clientes
  resources :midias
  resources :filmes
  resources :categorias
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
