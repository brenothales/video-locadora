class CreateFilmes < ActiveRecord::Migration[5.0]
  def change
    create_table :filmes do |t|
      t.references :categoria, foreign_key: true
      t.string :descricao
      t.integer :ano

      t.timestamps
    end
  end
end
