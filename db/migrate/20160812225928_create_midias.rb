class CreateMidias < ActiveRecord::Migration[5.0]
  def change
    create_table :midias do |t|
      t.references :filme, foreign_key: true
      t.boolean :inutilizada

      t.timestamps
    end
  end
end
