class CreateLocacoes < ActiveRecord::Migration[5.0]
  def change
    create_table :locacoes do |t|
      t.references :cliente, foreign_key: true
      t.references :midia, foreign_key: true
      t.date :dia_emprestimo
      t.date :dia_devolucao
      t.string :observacao

      t.timestamps
    end
  end
end
