# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160812230304) do

  create_table "categorias", force: :cascade do |t|
    t.string   "descricao"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "clientes", force: :cascade do |t|
    t.string   "nome"
    t.string   "telefone"
    t.string   "celular"
    t.string   "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "enderecos", force: :cascade do |t|
    t.string   "rua"
    t.string   "bairro"
    t.string   "numero"
    t.string   "cidade"
    t.string   "estado"
    t.string   "cep"
    t.string   "complemento"
    t.integer  "cliente_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["cliente_id"], name: "index_enderecos_on_cliente_id"
  end

  create_table "filmes", force: :cascade do |t|
    t.integer  "categoria_id"
    t.string   "descricao"
    t.integer  "ano"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["categoria_id"], name: "index_filmes_on_categoria_id"
  end

  create_table "locacoes", force: :cascade do |t|
    t.integer  "cliente_id"
    t.integer  "midia_id"
    t.date     "dia_emprestimo"
    t.date     "dia_devolucao"
    t.string   "observacao"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["cliente_id"], name: "index_locacoes_on_cliente_id"
    t.index ["midia_id"], name: "index_locacoes_on_midia_id"
  end

  create_table "midias", force: :cascade do |t|
    t.integer  "filme_id"
    t.boolean  "inutilizada"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["filme_id"], name: "index_midias_on_filme_id"
  end

end
