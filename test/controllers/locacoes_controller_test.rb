require 'test_helper'

class LocacoesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @locacao = locacoes(:one)
  end

  test "should get index" do
    get locacoes_url
    assert_response :success
  end

  test "should get new" do
    get new_locacao_url
    assert_response :success
  end

  test "should create locacao" do
    assert_difference('Locacao.count') do
      post locacoes_url, params: { locacao: { cliente_id: @locacao.cliente_id, dia_devolucao: @locacao.dia_devolucao, dia_emprestimo: @locacao.dia_emprestimo, midia_id: @locacao.midia_id, observacao: @locacao.observacao } }
    end

    assert_redirected_to locacao_url(Locacao.last)
  end

  test "should show locacao" do
    get locacao_url(@locacao)
    assert_response :success
  end

  test "should get edit" do
    get edit_locacao_url(@locacao)
    assert_response :success
  end

  test "should update locacao" do
    patch locacao_url(@locacao), params: { locacao: { cliente_id: @locacao.cliente_id, dia_devolucao: @locacao.dia_devolucao, dia_emprestimo: @locacao.dia_emprestimo, midia_id: @locacao.midia_id, observacao: @locacao.observacao } }
    assert_redirected_to locacao_url(@locacao)
  end

  test "should destroy locacao" do
    assert_difference('Locacao.count', -1) do
      delete locacao_url(@locacao)
    end

    assert_redirected_to locacoes_url
  end
end
